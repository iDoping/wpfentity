﻿using System.Data.Entity;
using System.Windows;
using WpfEntity.Model;

namespace WpfEntity
{
    public partial class MainWindow : Window
    {
        artAndconcdbEntities context = new artAndconcdbEntities();
        public MainWindow()
        {
            InitializeComponent();
            context.Artiste.Load();
            artisteGrid.ItemsSource = context.Artiste.Local.ToBindingList();
        }

        private void Delete_Artiste_Click(object sender, RoutedEventArgs e)
        {
            if (artisteGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < artisteGrid.SelectedItems.Count; i++)
                {
                    Artiste artiste = artisteGrid.SelectedItems[i] as Artiste;
                    if (artiste != null)
                    {
                        context.Artiste.Remove(artiste);
                    }
                }
            }
            context.SaveChanges();
        }

        private void Update_Artist_Click(object sender, RoutedEventArgs e)
        {
            context.SaveChanges();
        }

        private void Show_ConcertWindow_Click(object sender, RoutedEventArgs e)
        {
            ConcertWindow window = new ConcertWindow();
            window.Show();
        }
    }
}