﻿using System.Collections.Generic;
using System.Windows;
using WpfEntity.Model;
using System;


namespace WpfEntity
{
    public partial class ProcedureWindow : Window
    {
        artAndconcdbEntities context = new artAndconcdbEntities();
        public ProcedureWindow()
        {
            InitializeComponent();
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            string artName = ArtNameText.Text;
            int pageNum = Int32.Parse(PageNum.Text);
            int cellsCount = Int32.Parse(CellsCount.Text);;
            var storProc = context.GetConcertsByArteste(artName,pageNum,cellsCount);
            List<GetConcertsByArteste_Result> resList = new List<GetConcertsByArteste_Result>();
            foreach (GetConcertsByArteste_Result concerts in storProc)
            {
                resList.Add(concerts);
            }
            ProcGrid.ItemsSource = resList;
        }
    }
}
