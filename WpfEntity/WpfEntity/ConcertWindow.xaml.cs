﻿using System.Data.Entity;
using System.Windows;
using WpfEntity.Model;

namespace WpfEntity
{
    public partial class ConcertWindow : Window
    {
        artAndconcdbEntities context = new artAndconcdbEntities();
        public ConcertWindow()
        {
            InitializeComponent();
            context.Concert.Load();
            concerteGrid.ItemsSource = context.Concert.Local.ToBindingList();
        }

        private void Delete_Concert_Click(object sender, RoutedEventArgs e)
        {
            if (concerteGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < concerteGrid.SelectedItems.Count; i++)
                {
                    Concert concert = concerteGrid.SelectedItems[i] as Concert;
                    if (concert != null)
                    {
                        context.Concert.Remove(concert);
                    }
                }
            }
            context.SaveChanges();
        }

        private void Update_Concert_Click(object sender, RoutedEventArgs e)
        {
            context.SaveChanges();
        }

        private void Show_ProcWindow_Click(object sender, RoutedEventArgs e)
        {
            ProcedureWindow procedureWindow = new ProcedureWindow();
            procedureWindow.Show();
        }
    }
}
