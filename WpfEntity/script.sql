USE [artAndconcdb]
GO
/****** Object:  Table [dbo].[Artiste]    Script Date: 23.07.2021 14:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Artiste](
	[ArtisteId] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Artiste] PRIMARY KEY CLUSTERED 
(
	[ArtisteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Concert]    Script Date: 23.07.2021 14:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Concert](
	[ConcertId] [int] NOT NULL,
	[ConcertCall] [nvarchar](max) NULL,
	[ConcertDate] [datetime] NULL,
 CONSTRAINT [PK_Concert] PRIMARY KEY CLUSTERED 
(
	[ConcertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Links]    Script Date: 23.07.2021 14:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Links](
	[LinkId] [int] IDENTITY(1,1) NOT NULL,
	[Aid] [int] NOT NULL,
	[Cid] [int] NOT NULL,
 CONSTRAINT [PK_Links] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Artiste] ([ArtisteId], [Name]) VALUES (1, N'alesso')
INSERT [dbo].[Artiste] ([ArtisteId], [Name]) VALUES (2, N'lady gaga')
INSERT [dbo].[Artiste] ([ArtisteId], [Name]) VALUES (3, N'adele')
INSERT [dbo].[Artiste] ([ArtisteId], [Name]) VALUES (4, N'illenium')
GO
INSERT [dbo].[Concert] ([ConcertId], [ConcertCall], [ConcertDate]) VALUES (1, N'Test', CAST(N'2021-10-25T11:00:00.000' AS DateTime))
INSERT [dbo].[Concert] ([ConcertId], [ConcertCall], [ConcertDate]) VALUES (2, N'record', CAST(N'2021-12-10T22:00:00.000' AS DateTime))
INSERT [dbo].[Concert] ([ConcertId], [ConcertCall], [ConcertDate]) VALUES (3, N'SilverStone', CAST(N'2020-05-16T19:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Links] ON 

INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (1, 2, 1)
INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (2, 3, 1)
INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (3, 4, 1)
INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (4, 4, 3)
INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (5, 4, 2)
INSERT [dbo].[Links] ([LinkId], [Aid], [Cid]) VALUES (6, 3, 3)
SET IDENTITY_INSERT [dbo].[Links] OFF
GO
ALTER TABLE [dbo].[Links]  WITH CHECK ADD  CONSTRAINT [FK_Links_Artiste] FOREIGN KEY([Aid])
REFERENCES [dbo].[Artiste] ([ArtisteId])
GO
ALTER TABLE [dbo].[Links] CHECK CONSTRAINT [FK_Links_Artiste]
GO
ALTER TABLE [dbo].[Links]  WITH CHECK ADD  CONSTRAINT [FK_Links_Concert] FOREIGN KEY([Cid])
REFERENCES [dbo].[Concert] ([ConcertId])
GO
ALTER TABLE [dbo].[Links] CHECK CONSTRAINT [FK_Links_Concert]
GO
/****** Object:  StoredProcedure [dbo].[GetConcertsByArteste]    Script Date: 23.07.2021 14:33:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetConcertsByArteste]
	(@artName nvarchar(50) = '',
	 @PageNumber int,
	 @RowsOfPage int)
AS
select Artiste.Name, Concert.ConcertCall,Concert.ConcertDate from Artiste,Concert,Links where Artiste.ArtisteId = Links.Aid and Concert.ConcertId = Links.Cid and Artiste.Name like '%'+@artName+'%' order by Artiste.Name
OFFSET (@PageNumber-1)*@RowsOfPage ROWS
FETCH NEXT @RowsOfPage ROWS ONLY
GO
